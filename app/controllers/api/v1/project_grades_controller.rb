class Api::V1::ProjectGradesController < ApplicationController
  def create
    project_grade = ProjectGrade.new(project_grades_params)
    if project_grade.save
      render json: project_grade
    else
      render json: { errors: project_grade.errors.full_messages }, status: :bad_request
    end
  end

  def destroy
    project_grade = ProjectGrade.find(params[:id])
    project_grade.archive
  end

  private

    def project_grades_params
      params.permit(:value, :project_id)
    end
end
