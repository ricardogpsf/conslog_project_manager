class Api::V1::ProjectsController < ApplicationController
  def index
    projects = Project.active.includes(:project_grades)
    render json: projects.to_json(include: :project_grades)
  end

  def create
    project = Project.new(projects_params)
    if project.save
      render json: project
    else
      render json: { errors: project.errors.full_messages }, status: :bad_request
    end
  end

  def update
    project = Project.find(params[:id])
    if project.update(projects_params)
      render json: project
    else
      render json: { errors: project.errors.full_messages }, status: :bad_request
    end
  end

  def destroy
    if params[:id].to_i > 0
      projects = [Project.find(params[:id])]
    else
      ids = params[:id].gsub(/[\[\]]/, '').split(',')
      projects = Project.find(ids)
    end
    projects.each(&:archive)
  end

  def done
    project = Project.find(params[:id])
    project.done!
    render json: project
  end

  private

    def projects_params
      params.permit(:name, :client_id)
    end
end
