class Project < ApplicationRecord
  include Archivable

  enum status: [ :done ]

  validates_presence_of :name, :client_id

  has_many :project_grades, -> { where(archived: false).order('project_grades.created_at DESC') }
  has_many :all_project_grades, class_name: 'ProjectGrade'

  scope :active, -> { where(archived: false).order('created_at DESC') }

  def done!
    update(status: :done, done_at: Time.now)
  end
end
