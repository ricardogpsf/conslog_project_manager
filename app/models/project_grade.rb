class ProjectGrade < ApplicationRecord
  include Archivable

  enum value: [ :a, :b, :c ]

  validates_presence_of :value, :project_id

  belongs_to :project
end
