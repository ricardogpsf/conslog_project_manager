module Archivable
  def archive
    update(archived: true, archived_at: Time.now)
  end
end
