FROM ruby:2.4.2

COPY ./Gemfile* /

RUN gem install bundler
RUN bundle install

EXPOSE 3000:3000