class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.references :client
      t.datetime :done_at
      t.integer :status, limit: 1
      t.boolean :archived, default: false, index: true
      t.datetime :archived_at
      t.timestamps
    end
  end
end
