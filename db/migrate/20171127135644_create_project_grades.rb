class CreateProjectGrades < ActiveRecord::Migration[5.1]
  def change
    create_table :project_grades do |t|
      t.integer :value, null: false
      t.references :project, null: false
      t.boolean :archived, default: false, index: true
      t.datetime :archived_at
      t.timestamps
    end
  end
end
