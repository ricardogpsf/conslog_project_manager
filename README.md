## Conslog Project Manager

## Development setup

We are using docker for our development enviroment. =)

So, you need install docker (https://docs.docker.com/engine/installation/) to run our app, or if you want, you can install all dependencies by hands. =(

- Ruby 2.4
- Rails 5.1

### Commands to create the docker environment for development:

#### build a image
`$ docker image build --rm -t conslog-image .`

#### run a new container based on the image created:
`$ docker run --rm --name container_name -v "$PWD":/app -w /app -p 3000:3000 -it conslog-image bash`

### To create a database for development and tests
`$ bundle exec rails db:setup` inside docker machine

This command just need be executed at first time after the built the container docker.

### To run tests
`$ bundle exec rspec` inside docker machine

This command will execute rubocop (a Ruby static code analyzer) and simplecov (Code coverage for Ruby), too.

You can check the code coverage at `coverage/index.html`

### To execute the app
`$ rails s -b 0.0.0.0` inside docker machine and access `https://localhost:3000`

## API Endpoints

- URL base: `/api/v1/`

### GET /projects

Returns all projects not archived and its grades not archived

### POST /projects

##### request:

```json
{
  name: 'Project 1', // name of the project
  client_id: 1       // id of the client
}
```

### PUT /projects/:id

##### request:

```json
{
  name: 'Project 1', // name of the project
  client_id: 1       // id of the client
}
```

### DELETE /projects/:id

Archives the project

Note: You can pass an array of ids to the request, and all projects of ids will be archived. E.g: `api/v1/projects/[1,2,3]`

### PATCH /projects/:id

Marks the project as done

### POST /project_grades

##### request:

```json
{
  value: 'a',    // can be 'a', 'b' or 'c'
  project_id: 1  // id of the project
}
```

### DELETE /project_grades/:id

Archives the grade
