FactoryBot.define do
  factory :project do
    name 'Project 1'
    client_id 1
  end

  factory :project_archived, class: Project do
    name 'Project 2'
    client_id 2
    archived true
    archived_at Time.now
  end
end
