require 'rails_helper'

RSpec.describe Api::V1::ProjectsController, type: :controller do
  describe "GET index" do
    before do
      project = create(:project, created_at: Time.now - 1.days)
      create(:project_archived)
      create(:project_grade, project: project, archived: false, created_at: Time.now - 1.days)
      create(:project_grade, project: project, archived: true)
    end

    it "should return only the projects and project grades not archived" do
      get :index
      body = JSON.parse(response.body)
      expect(body.size).to eq(1)
      expect(body.first['archived']).to eq(false)

      grades = body.first['project_grades']
      expect(grades.size).to eq(1)
      expect(grades.first['archived']).to eq(false)
    end

    it "should return projects and project_grades ordered by created date DESC" do
      now = Time.now
      three_days_ago = now - 3.days
      yesterday = now - 1.days

      create(:project_grade, project: Project.first, created_at: now)
      create(:project, created_at: three_days_ago)

      get :index
      body = JSON.parse(response.body)

      dates = body.map { |project| project['created_at'].to_date }
      expect(dates).to eq([yesterday.to_date, three_days_ago.to_date])

      grades = body.first['project_grades']
      grades_dates = grades.map { |grade| grade['created_at'].to_date }
      expect(grades_dates).to eq([now.to_date, yesterday.to_date])
    end
  end

  describe "POST create" do
    context 'when request has valid attributes' do
      it 'should return a new project created' do
        post :create, params: attributes_for(:project)
        expect(response.status).to eq(200)
        expect(response.body).to match('Project 1')
        expect(Project.count).to eq(1)
      end
    end

    context 'when request has NOT valid attributes' do
      it 'should return a error message' do
        post :create, params: {}
        expect(response.status).to eq(400)
        body = JSON.parse(response.body)
        expect(body['errors']).to match(["Name can't be blank", "Client can't be blank"])
        expect(Project.count).to eq(0)
      end
    end
  end

  describe "PUT update" do
    before do
      create(:project)
    end

    context 'when request has valid attributes' do
      it 'should return a project updated' do
        put :update, params: { id: 1, name: 'Project Changed', client_id: 1000 }
        expect(response.status).to eq(200)
        expect(response.body).to match('Project Changed')
        expect(Project.count).to eq(1)
      end
    end

    context 'when request has NOT valid attributes' do
      it 'should return a error message' do
        put :update, params: { id: 1, name: '', client_id: 1000 }
        expect(response.status).to eq(400)
        body = JSON.parse(response.body)
        expect(body['errors']).to match(["Name can't be blank"])
        expect(Project.count).to eq(1)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      create(:project, id: 1)
      create(:project, id: 2)
    end

    context 'when pass just one project inside request' do
      it 'should archive the project' do
        delete :destroy, params: { id: 1 }
        expect(response.status).to eq(204)
        expect(Project.count).to eq(2)
        expect(Project.find(1).archived?).to eq(true)
        expect(Project.find(1).archived_at.to_date).to eq(Time.now.to_date)
        expect(Project.find(2).archived?).to eq(false)
      end
    end

    context 'when pass multiple ids of projects inside request' do
      it 'should archive all projects' do
        delete :destroy, params: { id: '[1, 2]' }
        expect(response.status).to eq(204)
        expect(Project.count).to eq(2)
        expect(Project.find(1).archived?).to eq(true)
        expect(Project.find(2).archived?).to eq(true)
      end
    end
  end

  describe 'PATCH done' do
    before do
      create(:project, id: 1)
    end

    it 'should mark the project as done' do
      patch :done, params: { id: 1 }
      expect(response.status).to eq(200)
      expect(Project.first.status).to eq('done')
      expect(Project.first.done_at.to_date).to eq(Time.now.to_date)
    end
  end
end
