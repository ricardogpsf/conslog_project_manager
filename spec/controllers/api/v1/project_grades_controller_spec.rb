require 'rails_helper'

RSpec.describe Api::V1::ProjectGradesController, type: :controller do
  describe "POST create" do
    before do
      create(:project, id: 1)
    end

    context 'when request has valid attributes' do
      it 'should return a new project_grade created' do
        post :create, params: { value: :a, project_id: 1 }
        expect(response.status).to eq(200)
        expect(ProjectGrade.count).to eq(1)
        expect(ProjectGrade.first.project).to be
      end
    end

    context 'when request has NOT valid attributes' do
      it 'should return a error message' do
        post :create, params: {}
        expect(response.status).to eq(400)
        body = JSON.parse(response.body)
        expect(body['errors']).to include("Value can't be blank", "Project can't be blank")
        expect(ProjectGrade.count).to eq(0)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      create(:project_grade, id: 1)
    end

    it 'should archive the project' do
      delete :destroy, params: { id: 1 }
      expect(response.status).to eq(204)
      expect(ProjectGrade.count).to eq(1)
      expect(ProjectGrade.find(1).archived?).to eq(true)
      expect(ProjectGrade.find(1).archived_at.to_date).to eq(Time.now.to_date)
    end
  end
end
