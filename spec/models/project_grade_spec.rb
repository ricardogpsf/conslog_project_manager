require 'rails_helper'

RSpec.describe ProjectGrade, type: :model do
  subject(:target) { create(:project_grade) }

  it_behaves_like 'ValidationsSpec', [:value, :project_id, :project]
  it_behaves_like 'ArchivableSpec'

  describe 'creating a valid project_grade' do
    it 'should work well' do
      project = create(:project)
      project_grade = ProjectGrade.new(project_id: project.id, value: :b)

      expect(project_grade.save).to eq(true)
      expect(project_grade.archived?).to eq(false)
      expect(project_grade.project).to eq(Project.find(project.id))
      expect(project_grade.value).to eq('b')
    end
  end
end
