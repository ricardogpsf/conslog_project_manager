require 'rails_helper'

RSpec.describe Project, type: :model do
  subject(:target) { create(:project) }

  it_behaves_like 'ValidationsSpec', [:name, :client_id]
  it_behaves_like 'ArchivableSpec'

  describe '#done!' do
    let(:project) { create(:project) }
    it 'should mark the project as done and set the conclusion date' do
      expect(project.done?).to eq(false)
      expect(project.status).to eq(nil)
      expect(project.done_at).to eq(nil)

      project.done!

      expect(project.done?).to eq(true)
      expect(project.status).to eq('done')
      expect(project.done_at.to_date).to eq(Time.now.to_date)
    end
  end
end
