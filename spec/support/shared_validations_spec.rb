RSpec.shared_examples 'ValidationsSpec' do |attributes|
  describe 'rails validations to model' do
    it 'should be correct' do
      empty_target = described_class.new
      expect(empty_target.save).to eq(false)
      expect(empty_target.errors).not_to be_empty
      expect(empty_target.errors.messages.keys).to contain_exactly(*attributes)
    end
  end
end
