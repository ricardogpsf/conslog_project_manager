# Note: you should create a variable called target inside the spec being executed
#  to be used by this shared example
RSpec.shared_examples 'ArchivableSpec' do
  describe '#archive' do
    it 'should mark the resource as archived and set the archived date' do
      expect(target.archived?).to eq(false)
      expect(target.archived_at).to eq(nil)

      target.archive

      expect(target.archived?).to eq(true)
      expect(target.archived_at.to_date).to eq(Time.now.to_date)
    end
  end
end
